/* [parameters:] */
// what to make
make="cableholder"; // [platform, cableholder]
// outer diameter
outer_diameter=90;
// inner diameter
inner_diameter=40;
// platform height
platform_height = 4;
// slot width 
slot_width=4;
// printer adjustment
adjustment=0.1;

// this hides the rest of the variables from the customizer
module __Customizer_Limit__ () {}

$fn=64;

if(make=="platform") platform();
if(make=="cableholder") cableholder();
module platform(){
    difference() {
        difference(){
            linear_extrude(height=platform_height){
            half_pie(outer_diameter, inner_diameter);
        }
        for(r=[15:30:165])
            slot(r);
        }
        connector(-1);
    }
    connector();
}

module mirror_copy(v=[1,0,0]) {
    children();
    mirror(v) children();
}


module cableholder(){
    inner_length=40;
    inner_width=12;
    mirror_copy([0,1,0])  {
        
        mirror([0,1,0]) {
            translate([0,-1*(12+inner_width),0])
            polygon(points = [
                [0,2],[2,0],[8,0],[8,3], [7,4],[3,4],[3,8],[7,8],[8,9],[8,11],[12,15],
                [12+inner_length,15],[12+inner_length+4,11],[12+inner_length+4,9],
                [12+inner_length+5,8],[12+inner_length+9,8],
                [12+inner_length+9,4],[12+inner_length+5,4],
                [12+inner_length+4,3],[12+inner_length+4,0],
                [12+inner_length+10,0],[12+inner_length+12,2],
                [12+inner_length+12,10],[12+inner_length+19, 10],
                [12+inner_length+21,12],[12+inner_length+21,12+inner_width],
                [12+inner_length+19,12+inner_width],[0,12+inner_width]
            ]);
        }
    }
    echo(12+inner_length+19);
    echo(12+inner_width);

}
module connector(negative=1){
    adjust = adjustment*negative*-1;
    echo(adjust);
    center = inner_diameter +(outer_diameter - inner_diameter)/2;
    translate([center*negative,-2*negative,0]) 
        cylinder(platform_height,4+adjust,4+adjust);
}
module pie_slice(r=3.0,a=30) {
    
    intersection() {
    circle(r=r);
    square(r);
  }
}

module quarter_pie(outer=90,inner=60) {
    difference(){
        pie_slice(outer, 90);
        pie_slice(inner, 90);
    }
}

module half_pie(outer=90, inner=60) {
    quarter_pie(outer, inner);
    rotate(90) quarter_pie(outer, inner);
}


module slot(angle=30) {
    //translate([platform_height/2,0,2]){
    translate([0,0,platform_height]) {rotate([0,0,angle]){
        cube([outer_diameter*2,slot_width,slot_width],center=true);
    }
}
}