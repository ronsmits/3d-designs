$fn = 50;

distance = 103;
thickness = 2;
height = 10;
pinradius = 2;
pinheight=3;
lockradius = 3;
lockheight = 2;
skadisthickness = 5.5;


translate([-20,-5.5,0.5]) {connecthook();}
translate([20,-5.5,0.5]) connecthook();

blockhook();


module connecthook() {
  difference() {
    translate([0,0,1])
    cube([ 4, skadisthickness * 2, skadisthickness * 2+2], true);
    translate([ -5.5 / 2, 0, 0 ])
        cube([ 4 + 1, skadisthickness + 1, skadisthickness *2  ]);
  }
}
module blockhook() {
  cube([ distance + 20, thickness, height ], center = true);

  translate(v = [ distance / 2, thickness / 2, 0 ]) { hook(); }
  translate([ -1 * distance / 2, thickness / 2, 0 ]) { hook(); }
}
module hook() {
  rotate([ 0, 90, 90 ]) {
    cylinder(h = pinheight, r = pinradius);
    translate([ 00, 0, pinheight ]) cylinder(h = lockheight, r = lockradius);
  }
}