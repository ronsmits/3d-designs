// facet number, 50 should be enough when printing
$fn = 50;

innerwidth = 135;
innerheight = 50;
innerlength = 70;
wallthickness = 5;
fillet = 1;
correction = 0.5;

bottompiece();
translate([ innerwidth * 2, 0, 0 ]) lid();
/**
Create the lid of the box
**/
module lid() {
  difference() {
    minkowski() {
      cube([ innerwidth, innerlength, wallthickness * 2 ], center = true);
      sphere(r = wallthickness);
    }
    // remove the top
    translate([ 0, 0, wallthickness ]) {
      cube([ innerwidth + 10, innerlength + 10, wallthickness * 2 ],
           center = true);
    }

    translate([ 0, 0, -2 ]) {
      difference() {
        linear_extrude(height = 10) minkowski() {
#square([ innerwidth + 4, innerlength + 4 ], true);
          circle(r = wallthickness);
        }
        linear_extrude(height = 10) minkowski() {
          square([ innerwidth + correction - 2, innerlength + correction - 2 ],
                 true);
          circle(r = wallthickness);
        }
      }
    }
  }
}

/**
    Creates the bottom piece of the box
**/
module bottompiece() {
  difference() {
    // the basic box
    minkowski() {
      cube([ innerwidth, innerlength, innerheight ], center = true);
      sphere(wallthickness);
    }

    // remove the top
    translate([ 0, 0, innerheight ]) {
      cube([ innerwidth + 10, innerlength + 10, innerheight ], center = true);
    }

    // hollow it out
    minkowski() {
      cube(
          [
            innerwidth - wallthickness, innerlength - wallthickness,
            innerheight -
            wallthickness
          ],
          center = true);
      sphere(wallthickness);
    }

    // make the inside edge for the lip from the top
    translate([ 0, 0, (innerheight / 2) - 2 ]) {
      linear_extrude(height = 10) minkowski() {
        square(
            [ innerwidth - wallthickness / 2, innerlength - wallthickness / 2 ],
            center = true);
        circle(r = wallthickness);
      }
    }
  }
}