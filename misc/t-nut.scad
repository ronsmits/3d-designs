// create a nut that fits a V slot
base=5.6;
lowervertical=1.44;
midhorizontal=1.8;
diagonalside=2.72;

module t_nut() {
   polygon(
       points=[
           [0,0],
           [base,0],
           [base,lowervertical],
           [base+midhorizontal,lowervertical],
           [base+midhorizontal-diagonalside, lowervertical+diagonalside],
           [base+midhorizontal-diagonalside-5, lowervertical+diagonalside],
           [-midhorizontal, lowervertical],
           [0, lowervertical]
       ]
   );
}

t_nut();