## Bed for Pedro

Simple design of a cheap bed (less then 50 euro). To open the design you need Freecad 0.19 with the "Assembly4 workbench installed.

I am re-using the slats from his old bed, so that is not added into the table below. I would use [these planks](https://www.hornbach.nl/shop/KONSTA-Vuren-lat-ruw-16-x-50-x-2100-mm/5185374/artikel.html) to build them. The space between the slats should be at least the same as the width of the slat (50mm in this case). The easiest way to calculate how many you need is by dividing the matrass length by 2 times the slat width, so in this case `200cm / 10cm = 20` which would add `20 * 1.18 = 23.60 euro` to the costs bringing it to a total of `44.33 + 23.60 = 67.93 euro`. As this is for a mattrass 200x160, you need 20 slats of 2100mm(the shortest length they have). If this was a single bed (mattrass of 200x90) you would need only 10.

Everything is parametric. There is a spreadsheet in `Bed.FCStd` where all the measurements are named. This is a technique I really like and I miss the `configurer` from OpenScad in this. 


| Item | Price | Amount | Total | Link |
|------|------:|-------:|------:|------|
|  Steigerhout | 6.95 | 4 | 27.80 |[Hornbach planks for sides and ends](https://www.hornbach.nl/shop/Steigerhout-plank-ca-32-x-200-x-2500-mm/8287177/artikel.html) |
|  zijsteunen | 2.08 | 2 | 4.16 | [Hornbach planks for side support](https://www.hornbach.nl/shop/KONSTA-Vuren-balk-geschaafd-27-x-44-x-ca-2100-mm/5185875/artikel.html) |
|  bedsteun | 4.99 | 1 | 4.99 |[sides and end connectors](https://www.meubelbeslagonline.nl/Ledikanthaken/bedhaken) |
|  midenbalk steun | 2.99 | 1 | 2.99 | [center beam support](https://www.meubelbeslagonline.nl/bedbeslag/middenbalk) |
|  middenbalk | 4.39 | 1 | 4.39 | [center support beam](https://www.hornbach.nl/shop/KONSTA-Vuren-balk-geschaafd-44-x-69-x-ca-2100-mm/5185887/artikel.html) |
|   |  | **total without slats** | **44.33** |  |
| lattenbodem | 1.18 | 20 | 23.60| [these planks](https://www.hornbach.nl/shop/KONSTA-Vuren-lat-ruw-16-x-50-x-2100-mm/5185374/artikel.html)
|   |  | **total** | **67.93** |  |