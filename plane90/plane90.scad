planeheight=20;
planelength=200;
against_plane_thickness=7;

bladeslot=20;
heightbelowblade=25;
underblade=20;
magnet_diameter=10;
magnet_height=5;

difference(){
    cube([planelength, planeheight, against_plane_thickness]);

    for(i = [1 :4])
        translate([planelength*(i/5), planeheight/2, against_plane_thickness-(magnet_height)]) {
            cylinder(d=magnet_diameter,h=magnet_height+1);
        }
}
translate([0, planeheight,0]) {
    cube([planelength*(2/3)-bladeslot/2, underblade, against_plane_thickness+heightbelowblade]);
}
translate([planelength*(2/3)+bladeslot/2, planeheight, 0]){
    cube([planelength*(1/3)-bladeslot/2, underblade, against_plane_thickness+heightbelowblade]);
}