

$fn=90;

base_radius=2;
total_length=81;
total_width=64;
total_height=2.5;

inner_base_length=60;
inner_base_width=23;
dovetail_width=10;

wall_thickness=3;
wall_distance=32;
wall_height=18;
wall_length=68;

base_inner_points = [
    [base_radius, base_radius, 0],
    [base_radius, wall_distance-5, 0],
    [wall_length, base_radius, 0],
    [wall_length, wall_distance-5, 0]
];

base(inner=true);
translate([(total_length-wall_length)/2, ((total_width-dovetail_width)-wall_distance)/2, 2.5])
wall();

module wall(){
    one_wall();
    translate([wall_length,wall_distance+wall_thickness,0]) rotate([0,0,180]) one_wall();
}

module one_wall(){
    cylinder(r=3, h=wall_height);
    cube([wall_length, wall_thickness, wall_height]);
    translate([wall_length,0,0])
cylinder(r=3, h=wall_height);
}

module base(inner=false) {
    union() {
        difference() {
            base_layout(inner);
            dovetail_holes();
        }
    }
    translate([0,total_width-dovetail_width, 0]) dovetail_holes(inner=0.1);
}

module dovetail_holes(inner=0){
    translate([total_length*(1/3), 0, 0])
        dovetail(inner);
    translate([total_length*(2/3),0,0])
        dovetail(inner);    
}

module dovetail(inner=0) {
    right_x=(5-inner)*sin(90-5);
    left_x=-1*right_x;
    
    linear_extrude(height=2.5)
        polygon([[0,0],[2.5-inner,0],
            [right_x,dovetail_width-inner],
            [left_x,dovetail_width-inner],
            [-2.5-inner,0],
            [0,0]]);
}

base_outer_points = [ 
    [0+base_radius, 0+base_radius, 0],
    [0+base_radius, total_width-dovetail_width-base_radius,0],
    [total_length-base_radius, 0+base_radius,0],
    [total_length-base_radius,total_width-dovetail_width-base_radius,0]
];



module base_layout(inner=true){
    if(inner==true)
        difference(){
            rounded_box(base_outer_points, base_radius, 2.5);
            translate([(total_length-wall_length-3)/2, 
                ((total_width-dovetail_width)-wall_distance)/2+3, 
                0])
                #rounded_box(base_inner_points, base_radius, 2.5);
        }
    else
        rounded_box(base_outer_points, base_radius, 2.5);
}

/*
 * found this on https://hackaday.com/2018/02/13/openscad-tieing-it-together-with-hull/
 */
module rounded_box(points, radius, height){
    hull(){
        for (p = points){
            translate(p) cylinder(r=radius, h=height);
        }
    }
}