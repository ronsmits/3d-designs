include <library.scad>
// select which part to draw, only right and left need to be printed. mount is a
// test
which_part = "right";  // [right, left, mount, both]
test_print = "yes";    // [yes, no]
// inner radius
radius = 125;
// width, determines the outer radius
width = 5;
// hole for the diameter
cable_diameter = 20;
height = 5;
// half of the lamphole distance
lamphole_distance = 100;

/* [Hidden] */
angles = [ 0, 90 ];
fn = 96;
connector_size = 5;
tolerance = 0.1;

if (which_part == "right") {
  rightpart();
}

if (which_part == "left") {
  leftpart();
}

if (which_part == "mount") {
  mountpart();
}
if (which_part == "both") {
  rightpart();
  leftpart();
}
x = Xis(radius / 2 - width);

module rightpart() {
  linear_extrude(height) arc(radius, angles, width, fn);
  cube([ width, radius, height ]);

  difference() {
    cube([ radius, width, height ]);
    if (test_print == "no") {
      translate([ radius / 2, width, height / 2 ]) {
        rotate([ 90, 0, 0 ]) cylinder(h = width, d = cable_diameter, $fn = fn);
      }
    }
  }
  if (test_print == "no") {
    // support and screw holder
    translate([ 0, radius / 2 - width, 0 ]) {
      difference() {
        cube([ Xis(radius / 2 - width), width * 2, 5 ]);
        translate([ x - radius / 5, width, 0 ]) {
          cylinder(h = 6, d = 5, $fn = fn);
        }
      }
    }
  }
  middlemount();
  // mountpart();
}

module leftpart() {
  linear_extrude(height) arc(radius, [ 90, 180 ], 5, fn);
  translate([ -radius, 0, 0 ]) cube([ radius, width, height ]);
  translate([ -width, 0, 0 ]) { cube([ width, radius, height ]); }

  // support and screw holder
  //    x = Xis(radius / 2 - width);

  if (test_print == "no") {
    translate([ -x, radius / 2 - width, 0 ]) {
      difference() {
        cube([ x, width * 2, 5 ]);
        translate([ radius / 5, width, 0 ]) cylinder(h = 6, d = 5, $fn = fn);
      }
    }
  }
  mirror([ 1, 0, 0 ]) middlemount();
}

module middlemount() {
  translate([ 0, radius - 30, height ]) {
    difference() {
      cylinder(r = 2, h = 2, $fn = fn);
      translate([ -3, 0, 0 ]) #cube(6, center = true);
    }
    translate([ 0, -2, 2 ]) cube([ 2, 6, 2 ]);
  }
}

module mountpart() {
  translate([ 0, radius / 2 - width, 0 ]) {
    difference() {
      cube([ lamphole_distance + lamphole_distance / 5, 2 * width, height ]);

      for (step = [0:lamphole_distance / 5:lamphole_distance]) {
        translate([ step, 0, 0 ])
            cube([ (lamphole_distance / 5) / 2, width * 2, height - 5 ]);
      }
    }
    translate([ lamphole_distance, width, height ]) {
      cylinder(h = 3, d = 3, $fn = fn);
      translate([ 0, 0, 3 ]) cylinder(h = 2, d = 5, $fn = fn);
    }
  }
}
// locate the correct location of X given the radius and the Y and pythagoras
function Xis(Y) = sqrt(radius ^ 2 - Y ^ 2);



