
$fn=90;
module fan() {
        difference() {
        cube([40,40,2.5]);
        translate([20,20,0]) {
        cylinder(d=27,h=3);
        translate([12,12,0]) cylinder(d=3,h=27);
        translate([-12,12,0]) cylinder(d=3,h=27);
            translate([-12,-12,0]) cylinder(d=3,h=27);
            translate([12,-12,0]) cylinder(d=3,h=27);
        }
    }
}

union() {
    difference() {
    stlfile();
    translate([1,10.5,-3.8]) cube([40,40,2.5]);
    }
   translate([1,10.5,-3.7]) {
    fan();
   }
}

module stlfile() {
    
translate([5,3,20])
import ("Top_H20_Slots_SM.stl");

}