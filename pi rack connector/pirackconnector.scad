

p1 = [
    [0,5],
    [5,0],
    [30,0],
    [30,5],
    [7.5,5],
    [5,7.5]
];
p2=[
    [5,37.5],
    [7.5,40],
    [30,40],
    [30,45],
    [5,45],
    [0,40]

];

linear_extrude(height=5)
    polygon(concat(p1,p2));