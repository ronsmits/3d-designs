$fn = 100;

/* the hooks for the board */
cube([4,4,16]);
cube([4,8,4]);

translate([39.5,0,0]) union() {
        cube([4,4,16]);
        cube([4,8,4]);    
}

half = (39.5+4)/2;
length=74.3;

translate([half-(length/2),8,0]) {
    color("red") cube([length,4,5.6]);
    translate([0,4,0]) cube([5.8,8,5.6]);
}
translate([half+(length/2)-5.8,12,0]) {
    color("cyan") cube([5.8,8,5.6]);
}

