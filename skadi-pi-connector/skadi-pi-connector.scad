import("Bottom_Base_SM.stl");

translate([25,-11.5,-5.2]){
    difference(){
        cube([10,10,5]);
        translate([2.1,2.0,-1])
            cube([6,8,7]);
    }
}

translate([25,57.5,-5.2]){
    difference(){
        cube([10,10,5]);
        translate([2.1,0.0,-1]){
            cube([6,8,7]);
        }
    }
}