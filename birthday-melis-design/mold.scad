lito_length=181;
lito_height=137;
lito_edge=8;
wood_thickness=14;
insert=5;

outerlength=(wood_thickness-insert);


points = [
    [0,0],
    [outerlength+lito_length+outerlength, 0],
    [outerlength+lito_length+outerlength, insert+lito_edge+3],
    [lito_length, insert+lito_edge+3],
    [lito_length, insert+lito_edge],
    [lito_length+outerlength, insert+lito_edge],
    [lito_length+outerlength,insert],
    [outerlength,insert],
    [outerlength,insert+lito_edge],
    [outerlength+insert, insert+lito_edge],
    [outerlength+insert,insert+lito_edge+3],
    [0,insert+lito_edge+3]

];
linear_extrude(height=2)
    polygon(points); 

translate([0,-3,0])
cube([outerlength+lito_length+outerlength, 3, 7]);

// translate([0, insert, 0]) {
//     cube([outerlength, lito_edge, 3]);
//}