lito_length=181;
lito_height=137;
lito_edge=8;
wood_thickness=14;
insert=5;

outerlength=(wood_thickness-insert);

points = [
    [0,0],
    [outerlength+lito_height+outerlength, 0],
    [outerlength+lito_height+outerlength, insert+lito_edge+3],
    [lito_height, insert+lito_edge+3],
    [lito_height, insert+lito_edge],
    [lito_height+outerlength, insert+lito_edge],
    [lito_height+outerlength,insert],
    [outerlength,insert],
    [outerlength,insert+lito_edge],
    [outerlength+insert, insert+lito_edge],
    [outerlength+insert,insert+lito_edge+3],
    [0,insert+lito_edge+3]

];
linear_extrude(height=2)
    polygon(points);
translate([0,-3,0])
cube([outerlength+lito_height+outerlength, 3, 7]);