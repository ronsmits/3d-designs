width=78;
depth=12;
height=50;

plug_distance=32.5;
thickness=1;

plug_width=6.75;
plug_hole=12.20;
connector_height=5;

$fn=50;

base_corners = [
    [0,0,0],
    [0,depth+thickness,0],
    [width+thickness*2,0,0],
    [width+thickness*2,depth+thickness,0]
];
module alternate_base() {
    difference(){
        hull() {
            for (p = base_corners) {
                translate(p) cylinder(r=1, h=height+thickness);
            }
        }
        translate([thickness, -1, thickness+1]) {
            #cube([width, depth+1, height+thickness]);
        }
    }
}

module base() {
    difference() {
        
        cube([thickness+width+thickness, thickness+depth, height]);    
        
        translate([thickness, -1, thickness]) {
            cube([width, depth+1, height]);
        }
        // translate([thickness+(width/2), depth/2, 0]) {
            // cube([plug_hole, plug_width,7],true); 
        // }
    }
}
module runit(){
    difference() {
        
        alternate_base();
        translate([thickness+(width/2), depth/2, 0]) {
            //cube([plug_hole+3, plug_width+3,10],true); 
            cube([plug_hole, plug_width, 12], true);
        }
    }

    translate([thickness+(width/2), depth/2, -1*connector_height/2]) {
        cable_connect();
    }
}
module cable_connect() {
    difference() {
        cube([plug_hole+3, plug_width+thickness, connector_height], true);
        cube([plug_hole, plug_width, connector_height+1], true);

    }
}
difference() {
translate([thickness+(width/2), 50/2, 0])
    rotate([45,0,0])
        cylinder(d=15, h=50, center=true);
    translate([thickness, -1, thickness+1]) {
            #cube([width, depth+1, height]);
        }
}
runit();
//#base();