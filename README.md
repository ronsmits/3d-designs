Playing with openscad and freecad, I make simple designs that I actually use or plan to use

## spooldrawer
My attempt to make a drawer system for empty filament spools that is customizable.

## pi-connector
stand for a pi box that can be connected to another stand

## pi rack connector
A simple shelf holder. It is not meant to hold kilo's of material, but it will
hold:
* the pies (4 at the moment)
* the hub 
* at least 2 hdd enclosures
* powerstrip

## 30 mm fan for raspberrypi 3
There are two designs on thingiverse that I absolutely love for raspberrypi cases. [pi 4 case](https://www.thingiverse.com/thing:3723561) and [pi 3 case](https://www.thingiverse.com/thing:3719217)

## planttafel connector
Simple connector to make a table with 3 straight pieces of wood