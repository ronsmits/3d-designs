outer_diameter = 197;
real_height = 70.01;
depth = 49.01;
thickness = 1.01;
dividers = 0; // [0,1,2]
with_pin = "no"; // ["no", "yes"]
/* [1 means a full height, 2 half height] */
no_drawers = 1; // [1,2]
/* [make it $fa = 1 and $fs = 1.75/2 for print] */
$fa = 12;
$fs = 2;

height = real_height / no_drawers;

/* [Hidden] */
cube_size = 6;
inner_radius = (outer_diameter / 2) - depth;
union() {
    difference() {
        union() {
            connector(true);
            intersection() {
                difference() {
                    drawer(0);
                    drawer(thickness);
                }
                cube([(outer_diameter / 2) + 1, (outer_diameter / 2) + 1, height + 5]);
            }
            endplanks();
            if (dividers == 1)
            divider(45);
            else if (dividers == 2) {
                divider(30);
                divider(60);
            }
        }
        connector(false);
    }
    if (with_pin == "yes")
    connector_pin();
    handle();

}

module handle() {
    translate([0, (outer_diameter / 2) - thickness, height - 2]) {
        cube([10, 8, 2]);
        rotate([0, 90, 0])
            translate([-1, 8, 0])
                cylinder(d = 5, h = 10, $fn=90);
    }
}
module connector_pin() {
    translate([(outer_diameter / 2) - cube_size - thickness, thickness, 0]) {
        translate([cube_size / 2, cube_size / 2, 0])
            difference() {
                cylinder(d = 2.9, h = real_height+2, $fn = 90);
                translate([0, 0, real_height+2 + 1.3])
                    #sphere(d = 3, $fn = 90);
                translate([0, 0, - 1.1])
                    #sphere(d = 3, $fn = 90);
            }
    }
}
module connector(no_cube = true) {
    translate([(outer_diameter / 2) - cube_size - thickness, thickness, 0]) {
        if (no_cube == true) cube([cube_size, cube_size, height]);
        else
            translate([cube_size / 2, cube_size / 2, - 1])
                cylinder(d = 3.3, h = height + 2, $fn = 90);

    }
}
module divider(angle = 45) {
    x = inner_radius * sin(angle);
    y = inner_radius * cos(angle);
    translate([x, y, 0]) {
        rotate([0, 0, 90 - angle])
            cube([depth - 0.1, thickness, height]);
    }
}

// create the sides on the X and the Y axis
module endplanks() {
    translate([inner_radius, 0, 0])
        cube([depth, thickness, height]);
    translate([0, inner_radius, 0])
        cube([thickness, depth, height]);
}

// Create the actual drawer
module drawer(wallthickness = 0) {
    translate([0, 0, wallthickness]) {
        difference() {
            cylinder(d = outer_diameter - (wallthickness * 2), h = height);
            cylinder(r = (outer_diameter / 2) - depth + wallthickness, h = height);
        }
    }
}