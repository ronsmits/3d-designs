dovetail_width=10;
$fn=90;
translate([20,0,0]){
    difference() {
    cube([30,30,20]);
    translate([15,0,17.5]) dovetail(inner=-0.1, height=2.5);
    }
}
connectionBlock();

points = [ [0,0,0], [30,0,0], [0,30,0], [30,30,0] ];
 
module rounded_box(points, radius, height){
    hull(){
        for (p = points){
            translate(p) cylinder(r=radius, h=height);
        }
    }
}


module connectionBlock() {
    cylinder(d=10, h=20);
    translate([0,2.5,20]) rotate([90,0,0])
    dovetail(height=5);
}


module dovetail(inner=0, height=2.5) {
    right_x=(5-inner)*sin(90-5);
    left_x=-1*right_x;
    
    linear_extrude(height=height)
        polygon([[0,0],[2.5-inner,0],
            [right_x,dovetail_width-inner],
            [left_x,dovetail_width-inner],
            [-2.5-inner,0],
            [0,0]]);
}